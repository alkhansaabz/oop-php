<?php

    // require("animal.php");
    require_once("frog.php");
    require_once("ape.php");

    $sheep = new Animal("shaun");

    echo "Name: $sheep->name <br>" ;
    echo "Legs: $sheep->legs <br>" ;
    echo "Cold Blooded: $sheep->coldblooded <br><br>" ;

    $kodok = new Frog("buduk");

    echo "Name: $kodok->name <br>" ;
    echo "Legs: $kodok->legs <br>" ;
    echo "Cold Blooded: $kodok->coldblooded <br>" ;
    echo "Jump: ";
    $kodok->jump();
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");

    echo "Name: $sungokong->name <br>" ;
    echo "Legs: $sungokong->legs <br>" ;
    echo "Cold Blooded: $sungokong->coldblooded <br>" ;
    echo "Yell: " ;
    $sungokong->yell();
